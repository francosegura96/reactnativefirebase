import React, { Component } from "react";
import { Text } from "react-native";
import firebase from "firebase";
import { Card, CardSection, Button, Input, Spinner } from "./common";

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      language: "",
      error: "",
      loading: false
    };
  }
  changeText = (att, value) => {
    this.setState({
      [att]: value
    });
  };

  onButtonPress = () => {
    const { email, password } = this.state;
    this.setState({
      error: "",
      loading: true
    });
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(this.onLoginSuccess.bind(this))
      .catch(() => {
        firebase
          .auth()
          .createUserWithEmailAndPassword(email, password)
          .then(this.onLoginSuccess.bind(this))
          .catch(this.onLoginFailed.bind(this));
      });
  };

  onLoginFailed(e) {
    console.log("Error: ", e);
    this.setState({
      error: "Authentication failed.",
      loading: false
    });
  }

  onLoginSuccess() {
    this.setState({
      email: "",
      password: "",
      language: "",
      error: "",
      loading: false
    });
  }

  render() {
    return (
      <Card>
        <CardSection>
          <Input
            onChangeText={text => this.changeText("email", text)}
            value={this.state.email}
            label={"Email"}
            placeholder={"Email"}
            secureTextEntry={false}
          />
        </CardSection>
        <CardSection>
          <Input
            onChangeText={text => this.changeText("password", text)}
            value={this.state.password}
            label={"Password"}
            placeholder={"Password"}
            secureTextEntry
          />
        </CardSection>
        <CardSection>
          <Text>{this.state.error}</Text>
        </CardSection>
        <CardSection>
          {this.state.loading ? (
            <Spinner size={"small"} />
          ) : (
            <Button onPress={this.onButtonPress.bind(this)}>Log in</Button>
          )}
        </CardSection>
      </Card>
    );
  }
}

export default LoginForm;
