import React, { Component } from "react";
import { View } from "react-native";
import firebase from "firebase";
import { Header, Button, Spinner, CardSection } from "./components/common";
import LoginForm from "./components/loginForm";

class App extends Component {
  state = {
    loggedIn: null
  }

  componentDidMount() {
    firebase.initializeApp({
      apiKey: "AIzaSyDyqpFMgrw9ZyNi4HdOgpkEXBcjn8hiSoo",
      authDomain: "auth-cebb1.firebaseapp.com",
      databaseURL: "https://auth-cebb1.firebaseio.com",
      projectId: "auth-cebb1",
      storageBucket: "",
      messagingSenderId: "95593525218",
      appId: "1:95593525218:web:55107517b8069bb2"
    });
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.setState({ loggedIn: true })
      }
      else {
        this.setState({ loggedIn: false })
      }
    })
  }

  renderContent() {
    console.log('Consola: ', this.state.loggedIn)
    switch (this.state.loggedIn) {
      case true:
        return (
          <CardSection>
            <Button onPress={() => firebase.auth().signOut()}>
              Sign out
          </Button>
          </CardSection>
        )
      case false:
        return <LoginForm />
      default:
        return <Spinner size='large' />
    }
  }

  render() {
    return (
      <View>
        <Header headerText={"Authentication"} />
        {this.renderContent()}
      </View>
    );
  }
}

export default App;
